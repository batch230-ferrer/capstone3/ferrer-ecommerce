import { Navigate } from 'react-router-dom'
import { useEffect, useContext } from 'react';
import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function Logout(){
    
    const { unsetUser, setUser} = useContext(UserContext);
    unsetUser();

    useEffect(() => {
        setUser({email: null, id: null});

    
        Swal.fire({
            title: "You've been logged out.",
            text: "Please log back in.",
            icon: "info",
            imageUrl: 'https://images.pexels.com/photos/225406/pexels-photo-225406.jpeg',
            imageWidth: 350,
            imageHeight: 350,
            imageAlt: 'Custom image',
          })
    })

    return(
        <Navigate to="/login"/>
    )
}