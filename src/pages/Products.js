
import ProductCard from '../components/ProductCard';
import { useContext, useEffect, useState } from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';

export default function Products(){

    const { user } = useContext(UserContext);
    const [ products, setProducts ] = useState([])

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/`)
        .then(res => res.json())
        .then(data => {
            console.log(data);
            setProducts(data.map(product => {
                return(
                    <ProductCard key={product._id} productProp = {product}/>
                )
            }))
        })
    }, []);

    return (
        (user.isAdmin)?
            <Navigate to ="/admin" />
        :
        <>
            
            {products}
        </>
    )
}