import { useContext, useState, useEffect } from "react";
import UserContext from '../UserContext';
import { Navigate, Link } from "react-router-dom";
import { Table, Container, Button } from "react-bootstrap";


export default function AdminOrderedProducts(){
    const { user } = useContext(UserContext);
    const [allOrders, setAllOrders] = useState([]);

    const cancel = () =>{

    }

    const fetchData = () =>{
		fetch(`${process.env.REACT_APP_API_URL}/users/:userId`, {
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
		console.log(data);
		setAllOrders(data.map(order => {
				return (
					<tr key={order._id}>
						<td className="text-light">{order.userId}</td>
						<td className="text-light">{order.userEmail}</td>
						<td className="text-light">{order.quantity}</td>
						<td className="text-light">{order.orderStatus ? "Active" : "Inactive"}</td>
						<td className="text-light text-center">
							<Button variant="danger" size="sm" onClick={() => cancel(order._id, order.name)}>Cancel Order</Button>
						</td>
					</tr>
				)
			}));
		});
	}
    useEffect(()=>{
		fetchData();
	}, [])

    return(
        <div className="text-white pt-5">
            <Container className="col-6">
                <div className="mb-3 text-center text-light">
                    <h1>Admin Dashboard(Ordered Products)</h1>
                    <Button variant="success" className="mx-2" as={Link} to="/admin">Back to main dashboard</Button>
                </div>
                <Table striped bordered hover className="bg-dark text-light">
                    <thead>
                        <tr className="text-center">
                        <th>User ID</th>
                        <th>User Email</th>
                        <th>Quantity</th>
                        <th>Status</th>
                        <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {allOrders}
                    </tbody>
                </Table>
                <div>
                    
                </div>
            </Container>
        </div>
    )
}
